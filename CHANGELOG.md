# CHANGELOG
## 3.0.2 - 2023-06-13

### Fixed

- Eviter des fatals lors de l'emploi du cache

## 3.0.1 - 2023-06-13

### Added

- Compatibilité SPIP 4.2 (sous réserve de la compatibilité du plugin `indexer`)
- Possibilité de navigation transversale par mot-clé/facette
- #3 Indiquer la date de modification

### Changed

- #1 Rendre facultatif l'emploi du plugin `notation`

### Fixed

- Dans les filtres par facette, afficher aussi les groupes de mots qui sont autorisés pour toutes les rubriques
- Appeler correctement `#FORMULAIRES_NOTATION`
- Afficher à nouveau l'auteur de la mise en ligne
- Retour des micron icones pour meta données

### Removed
- Dépendance à Macrosession

## 3.0.0 - 2022-01-31
### Removed
- Compatibilité SPIP 3.2
