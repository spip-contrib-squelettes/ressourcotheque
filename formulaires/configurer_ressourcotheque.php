<?php
if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 **/
function formulaires_configurer_ressourcotheque_saisies(){
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = array(
		array(
			'saisie' => 'groupe_mots',
			'options' => array(
				'nom' => 'groupe_mots_logos',
				'label' => _T('ressourcotheque:configurer_groupe_mots_logos_label'),
				'explication' => _T('ressourcotheque:configurer_groupe_mots_logos_explication'),
			)
		)
	);
	return $saisies;
}
